/*---------MODEL-----------------*/
var ref_int = 0,
    student_programmer = {
        first_name_str: "Rick",
        city_str: "San Francicso"
    },
    shopping_cart = {
        product_name: "Street fighter V",
        cost_in_cents_int: 4999
    },
    food = {
        food_is_here_boo: false
    };
/*---------VIEW------------------*/
function _visulize(type_str){
    //add really expressive view logic below
    switch(type_str){
        case "add":
            //append uls etc
            document.bgColor = "green";
            break;
        case "delete":
            //hide elements etc
            document.bgColor = "red";
            break;
        case "update":
            //show a change
            document.bgColor = "yellowgreen";
            break;
        default:
            throw "weird update!!";
    }
}
function letEveryoneKnowThereWasAChange(change){
    var prop_str = change["name"],
        latest_u = change.object[prop_str]; //could be any type
    console.log("There was a change of type " + change["type"] + " to " + change["name"]);
    
    //optional?
    _visulize(change["type"]);
        
    //deleted
    if(latest_u === undefined){
        console.info("It was " + change["oldValue"] + " and is now deleted");
        return;
    }
    
    //changed
    if(change["oldValue"] !== undefined){
        console.info("It was " + change["oldValue"] + " and is now " + latest_u);
        return;
    }
    
    //added
    console.log("It had no previous value but is now:");
    console.dir(latest_u);
}
function showCartIcon(){
    document.body.bgColor = "hotpink";
    console.info("cart updated");
}
/*---------CONTROLLER------------*/
function prepareToletEveryoneKnowThereWasAChange(changes_arr){
    console.log("Client app, here is a change");
    letEveryoneKnowThereWasAChange(changes_arr[0]);
}
function setUpObserver(){
    //ensure you have loaded NB first
    window.NB.observe(student_programmer, prepareToletEveryoneKnowThereWasAChange);
    window.NB.observe(shopping_cart, showCartIcon);
    window.NB.observe(food, function(change){
        console.log("food is here!");
        console.dir(change);
    });
}
function addASimpleBooleanProperty(key_name_str, boo){
    student_programmer["likes_coffee_boo"] = boo;
}
function makeAChangeInAFewSeconds(count_int){
    ref_int = setTimeout(function(){
        addASimpleBooleanProperty("likes_coffee_boo", true);
    }, count_int * 1000);
}
(function(){
    makeAChangeInAFewSeconds(6);
    setUpObserver();
})();