/*--------NECKBEARD---------------*/
/**
 * Only for shallow (single level has own properties)
 * booleans, string, and numbers //for now??
 * removing or replacing the entire object will not get tracked
 * 
 */
 
 
 /*
    Next steps create 2 arrays showing which objects are being observered
 
 */
var NB = (function(){
    var ref_int = null,
        change = null,
        wrapper_object_properties_arr = [],
        wrapper_object_values_arr = [],
        objects_to_observe_arr = [],
        callbacks_arr = [];
    function _removeItem(the_object_int, index_int){
        console.log("NB:An old property of " + wrapper_object_properties_arr[the_object_int][index_int]  + "was deleted");
        wrapper_object_properties_arr[the_object_int].splice(index_int, 1);
        wrapper_object_values_arr[the_object_int].splice(index_int, 1);
    }
    //pass in the index of the array of callbacks and objects
    function _quickCheck(the_object_int){
        //if there is a change
        Object.keys(objects_to_observe_arr[the_object_int]).forEach(function(item){
            //first check for an addition
            var index_int = wrapper_object_properties_arr[the_object_int].indexOf(item);
            if(index_int !== -1){
                // console.log("No new propery addions found, lets do nothing");
            }else{
                console.log("NB:A new property of " + item  + " was added with a value of " + objects_to_observe_arr[the_object_int][item]);
                wrapper_object_properties_arr[the_object_int].push(item); //object order does not matter
                wrapper_object_values_arr[the_object_int].push(objects_to_observe_arr[the_object_int][item]);
                change = {
                    type: "add",
                    name: item,
                    oldValue: wrapper_object_properties_arr[the_object_int][index_int],
                    object: objects_to_observe_arr[the_object_int]
                };
                return; //done never more than 1 change at a time
            }
            //check for a deletion
        });
        wrapper_object_properties_arr[the_object_int].forEach(function(item){
            var has_item= item in objects_to_observe_arr[the_object_int];
            if(has_item === true){
                // found keep going
            }else{
                // item has been deleted
                //do not modify yet
                var index_int = wrapper_object_properties_arr[the_object_int].indexOf(item);
                change = {
                    type: "delete",
                    name: item,
                    oldValue: wrapper_object_properties_arr[the_object_int][index_int],
                    object: objects_to_observe_arr[the_object_int]
                };
                _removeItem(the_object_int, index_int);
                return; //done never more than 1 change at a time
            }
        });
        Object.keys(objects_to_observe_arr[the_object_int]).forEach(function(item){
            //last check for change
            //wont work in NaN (never equals itself);
            //only on props
            var index_int = wrapper_object_properties_arr[the_object_int].indexOf(item);
            if(index_int !== -1){
                if(objects_to_observe_arr[the_object_int][item] === wrapper_object_values_arr[the_object_int][index_int]){
                    // console.log("no change on this item " + item + " it is still " + object_0[item]);
                }else{
                    //validate or rollback?
                    console.log("NB:The property" + item  + " was changed to a " + typeof objects_to_observe_arr[the_object_int][item] + " value of " + objects_to_observe_arr[the_object_int][item]);
                    change = {
                        type: "update",
                        name: item,
                        oldValue: wrapper_object_values_arr[the_object_int][index_int],
                        newValue: objects_to_observe_arr[the_object_int][item],
                        object: objects_to_observe_arr[the_object_int]
                    };
                    wrapper_object_values_arr[the_object_int][index_int] = objects_to_observe_arr[the_object_int][item]; //object order does not matter
                    return; //done never more than 1 change at a time
                }
                //check for change
               
               
                
            }else{
                //do nothing;
            }
        });
        if(change !== null){
            callbacks_arr[the_object_int]([change]);
            change = null;
        }
    }
    function _extractObjectInfo(){
        var object_to_observe = objects_to_observe_arr[objects_to_observe_arr.length - 1],
            // callback = callbacks_arr[callbacks_arr.length - 1],
            object_properties_arr = Object.keys(object_to_observe),
            object_values_arr = [];
        //array of arrays
        wrapper_object_properties_arr.push(object_properties_arr);
        wrapper_object_properties_arr[wrapper_object_properties_arr.length - 1].forEach(function(item){
            object_values_arr.push(object_to_observe[item]);
        });
        wrapper_object_values_arr.push(object_values_arr);
    }
    function _startFrequencyCheck(count_int){
        if(ref_int !== null){
            //already running a loop
        }else{
            //user request animation frame?
            ref_int = setInterval(function(){
                //set up a quickcheck for each item
                objects_to_observe_arr.forEach(function(item, the_object_int, arr){
                    // var the_object_int = objects_to_observe_arr.indexOf(item);
                    _quickCheck(the_object_int);
                });
            }, count_int * 1000);
        }
    }
    function observe(object_to_observe, callback){
        //add to arrs
        objects_to_observe_arr.push(object_to_observe);
        callbacks_arr.push(callback);
        // do nothin if they have this functionality (chrome);
        if(window.Object.observe !== undefined){
            console.info("Using Native O.o");
            window.Object.observe(object_to_observe, callback);
            return;
        }
        console.info("We are now watching this Object for shallow property changes");
        console.dir(object_to_observe);
         _extractObjectInfo();
         _startFrequencyCheck(0.1);
    }
    function unObserve(object_to_stop_observing){
        var the_object_int = objects_to_observe_arr.indexOf(object_to_stop_observing);
        console.log("I want to stop observing this object:");
        console.dir(objects_to_observe_arr[the_object_int]);
  
        //you remove this
        objects_to_observe_arr.splice(the_object_int, 1);
        callbacks_arr.splice(the_object_int, 1);
        wrapper_object_properties_arr.splice(the_object_int, 1);
        wrapper_object_values_arr.splice(the_object_int, 1);
        if(objects_to_observe_arr.length > 0 && ref_int && ref_int > 0){
            //keep this loop going
        }else{
            clearInterval(ref_int);
            ref_int = null;
            console.warn("stopping tracker as there are no more objects to observe");
        }
    }
    return {
        observe: observe,
        unObserve: unObserve
    };
})();